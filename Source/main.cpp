#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int RunTime = 0;

struct ItemsToBuy{
  string ItemName;
  int ItemID;
  float ItemPrice;
};

static ItemsToBuy ItemsArray[10];

class DataNode{
  friend class DataChain;
  private:
    int ItemID;
    int AmountSold;
    float TotalRevenue;

    DataNode *next;
};

class DataChain{
  private:
    DataNode *first; //pointer to first data node
    DataNode *last; //pointer to last data node
  public:
    DataChain(){first = last = 0;}
    int Length();
    DataChain& Insert(int ItemID, float Price); //Insert a new data node or update an old node
    DataChain& SortDataChain(); //Sort the Datachain - valtous se fthinousa sira
    void ShowSupermarketsData();
};

int DataChain :: Length(){
  //return the number of nodes in this chain
  DataNode *current = first;
  int len = 0;
  while(current){
    len++;
    current = current->next;
  }
  return len;
}

DataChain& DataChain :: SortDataChain(){

  DataNode *current = first;
  DataNode *temp = 0;

  while(current != last){
    temp = current->next;
    while(temp){
      if((temp->TotalRevenue) > (current->TotalRevenue)){
        int bucket1 = current->ItemID;
        int bucket2 = current->AmountSold;
        float bucket3 = current->TotalRevenue;

        current->ItemID = temp->ItemID;
        current->AmountSold = temp->AmountSold;
        current->TotalRevenue = temp->TotalRevenue;

        temp->ItemID = bucket1;
        temp->AmountSold = bucket2;
        temp->TotalRevenue = bucket3;

      }
      temp = temp->next;
    }
    current = current->next;
  }

  return *this;
}


DataChain& DataChain :: Insert(int Item_ID, float Price){
  DataNode *current = first;
  bool Flag = false;

  while(current){
    if(current->ItemID == Item_ID){ //Checks if items node already exists
      Flag = true;
      break;
    }
    current = current->next;
  }

  if(Flag == false){
    //Create new DataNode
    DataNode *y = new DataNode;
    y->ItemID = Item_ID;
    y->AmountSold = 1;
    y->TotalRevenue = Price;
    y->next = 0;

    if (first){
      //chain is not empty
      last->next = y;
      last = y;
    }else{
      //chain is empty
      first = last = y;
    }

  }else{
    current->TotalRevenue = current->TotalRevenue + Price;
    current->AmountSold = current->AmountSold + 1;
  }

  return *this;
}

/*
ItemID = 1 : Galaktokomika
ItemID = 2 : Zimarika
ItemID = 3 : Alantika
ItemID = 4 : Aporipantika
ItemID = 5 : Katharistika
*/

void DataChain :: ShowSupermarketsData(){
  DataNode *current = first;
  cout << "**********************************" << endl;
  cout << "Super Market Data" << endl;
  cout << "**********************************" << endl;
  while(current){
    if(current->ItemID == 1){
      cout << "Eidos: Galaktokomika" << endl;
    }else if(current->ItemID == 2){
      cout << "Eidos: Zimarika" << endl;
    }else if(current->ItemID == 3){
      cout << "Eidos: Alantika" << endl;
    }else if(current->ItemID == 4){
      cout << "Eidos: Aporipantika" << endl;
    }else{
      cout << "Eidos: Katharistika" << endl;
    }
    cout << "Amount Sold: " << current->AmountSold << endl;
    cout << "Total Revenue: " << current->TotalRevenue << "�" << endl;
    cout << "**********************************" << endl;
    current = current->next;
  }
}


DataChain Data;

class ItemNode{
  friend class ItemChain;
  private:
    int ItemID;
    string ItemName;
    float ItemPrice;

    ItemNode *next;
};


class ItemChain{
  friend class CustomerNode;
  private:
    ItemNode *first; //pointer to first item node
    ItemNode *last; //pointer to last item node

  public:
    ItemChain() {first = last = 0;}
    ~ItemChain();
    int Length();
    float ItemsTotalPrice();
    void ShowChain();
    void UpdateDataChain();
    ItemChain& Append(int magicNumber);
};

ItemChain :: ~ItemChain(){
  //Delete all nodes
  ItemNode *nextNode;
  while(first){
    nextNode = first->next;
    delete first;
    first = nextNode;
  }
}


int ItemChain :: Length(){
  //return the number of nodes in this chain
  ItemNode *current = first;
  int len = 0;
  while(current){
    len++;
    current = current->next;
  }
  return len;
}

void ItemChain :: UpdateDataChain(){
  ItemNode *current = first;
  while(current){
    Data.Insert(current->ItemID, current->ItemPrice);
    current = current->next;
  }
}

float ItemChain :: ItemsTotalPrice(){
  //return the total price of the items in list
  ItemNode *current = first;
  float sum = 0.0;
  for(current = first; current; current = current->next){
    sum += current->ItemPrice;
  }

  return sum;
}

  ItemChain& ItemChain :: Append(int magicNumber){
  //Create new ItemNode
  ItemNode *y = new ItemNode;
  y->ItemID = ItemsArray[magicNumber].ItemID;
  y->ItemName = ItemsArray[magicNumber].ItemName;
  y->ItemPrice = ItemsArray[magicNumber].ItemPrice;
  y->next = 0;

  if (first){
    //chain is not empty
    last->next = y;
    last = y;
  }else{
    //chain is empty
    first = last = y;
  }
  return *this;
}

void ItemChain :: ShowChain(){
  ItemNode *current;
  for (current = first; current; current = current->next){
    cout << "ItemID: " << current->ItemID << endl;
    cout << "ItemName: " << current->ItemName << endl;
    cout << "ItemPrice: " << current->ItemPrice << endl;
    cout << "----------------------------------------" << endl;
  }
}


class CustomerNode{
  friend class CustomerChain;
  private:
    CustomerNode *nextCustomer;
    int TotalItems;
    int ArrivalTime;
    int FirstTimeArrived;
    float TotalPrice;
    ItemChain L;

  public:
    CustomerNode();
    void WatchCustomersBucket(){L.ShowChain();}
    int GetTotalItems(){return TotalItems;}
    float GetTotalPrice(){return TotalPrice;}
    void UpdateGeneralData(){L.UpdateDataChain();}
};

CustomerNode :: CustomerNode(){
  nextCustomer = 0;
  TotalItems = 0;
  TotalPrice = 0.0;
  int NItems = rand() % 10 + 1; //The number of items will take
  for(int i=0; i<NItems; i++){
    int secretNumber = rand() % 10; //Picks an item from the ItemArray
    L.Append(secretNumber);
  }

  ArrivalTime = 0;
  FirstTimeArrived = RunTime;

  TotalItems = L.Length();
  TotalPrice = L.ItemsTotalPrice();

}

class CustomerChain{
  private:
    CustomerNode *firstCustomer; //pointer to first customer on the chain
    CustomerNode *lastCustomer; //pointer to last customer on the chain
    int ChainTotalItems;
    int TimeSum;
    int CCount;
    double AvgTime;

  public:
    CustomerChain(){firstCustomer = lastCustomer = 0;ChainTotalItems = 0;TimeSum = 0;CCount = 0;AvgTime = 0;}
    ~CustomerChain();
    CustomerChain& Append();
    CustomerChain& Delete();
    CustomerChain& AppendByForce(CustomerNode* nodePointer);
    CustomerChain& DropNode(int SelectNode);
    int GetTotalItems(){return ChainTotalItems;}
    int Length();
    double GetLinesAvgTime();
    void GetNodeInfo(int SelectNode, int &TotalItems, CustomerNode* &nodePointer);
    void CheckIfCustomerFinish();
    void ShowChainsDetails();
};

CustomerChain :: ~CustomerChain(){
  //delete all nodes in chain
  CustomerNode *nextNode;
  while(firstCustomer){
    nextNode = firstCustomer->nextCustomer;
    delete firstCustomer;
    firstCustomer = nextNode;
  }
}

void CustomerChain :: GetNodeInfo(int SelectNode, int &TotalItems, CustomerNode* &nodePointer){

  CustomerNode *current = 0;

  if(SelectNode == 0){
    current = firstCustomer;
    TotalItems = 0;
    nodePointer = current;
  }else{
    current = lastCustomer;
    TotalItems = current->TotalItems;
    nodePointer = current;
  }

}

int CustomerChain :: Length(){
  CustomerNode *current = firstCustomer;
  int len = 0;
  while(current){
    len++;
    current = current->nextCustomer;
  }
  return len;
}

double CustomerChain :: GetLinesAvgTime(){
  if(CCount == 0){
    return 0;
  }else{
    return TimeSum*1.0/CCount*1.0;
  }
}

CustomerChain& CustomerChain :: DropNode(int SelectNode){
  if(SelectNode == 1){
    //Drop last node
    CustomerNode *temp = firstCustomer;

    if(firstCustomer == lastCustomer){ //Only one node in chain
      ChainTotalItems -= temp->GetTotalItems();
      firstCustomer = lastCustomer = 0;
    }else{ //More than one nodes in chain
      for(int i=1; i < (Length()-1);i++){
        temp = temp->nextCustomer;
      }

      ChainTotalItems -= temp->nextCustomer->GetTotalItems();
      temp->nextCustomer = 0;
      lastCustomer = temp;
    }


  }else{

      CustomerNode *temp = firstCustomer;

      TimeSum += RunTime - (temp->FirstTimeArrived); //Update Average waiting time data
      CCount++;
      ChainTotalItems -= temp->GetTotalItems();

      if(firstCustomer == lastCustomer){
        firstCustomer = lastCustomer = 0;
      }else{
        firstCustomer = firstCustomer->nextCustomer;
        temp->nextCustomer = 0;
      }

  }

  return *this;

}

CustomerChain& CustomerChain :: AppendByForce(CustomerNode *nodePointer){
  ChainTotalItems += nodePointer->GetTotalItems();
  if(firstCustomer){
    //CustomerChain is not empty
    lastCustomer->nextCustomer = nodePointer;
    lastCustomer = nodePointer;
  }else{
    //CustomerChain is empty
    firstCustomer = lastCustomer = nodePointer;
    nodePointer->ArrivalTime = RunTime;
  }

  return *this;

}

CustomerChain& CustomerChain :: Append(){
  //Create new CustomerNode
  CustomerNode *y = new CustomerNode();
  y->nextCustomer = 0;
  ChainTotalItems += y->GetTotalItems();

 if(firstCustomer){
   //CustomerChain is not empty
   lastCustomer->nextCustomer = y;
   lastCustomer = y;
 }else{
   //CustomerChain is empty
   firstCustomer = lastCustomer = y;
   firstCustomer->ArrivalTime = RunTime;
 }

  return *this;
}

CustomerChain& CustomerChain :: Delete(){
  //delete the first costumer in the chain and update supermarkets data
  CustomerNode *temp = firstCustomer;
  if(temp != 0){
    temp->UpdateGeneralData(); //Update Data
    Data.SortDataChain();

    //Update AvgTime data
    TimeSum += RunTime - (temp->FirstTimeArrived);
    CCount++;

    //Delete Customer
    ChainTotalItems -= temp->GetTotalItems();
    firstCustomer = firstCustomer->nextCustomer;
    if(firstCustomer) firstCustomer->ArrivalTime = RunTime;
    delete temp;
  }
  return *this;
}

void CustomerChain :: CheckIfCustomerFinish(){
  CustomerNode *current = firstCustomer;
  if(current != 0){
    if(RunTime == (current->ArrivalTime) + 10*(current->TotalItems)){
      CustomerChain::Delete();
    }
  }
}

void CustomerChain :: ShowChainsDetails(){
  cout << "Customers waiting: " << Length() << endl;
  int TotalItemsInChain = 0;
  CustomerNode *current = firstCustomer;
  while(current){
    TotalItemsInChain += current->TotalItems;
    current = current->nextCustomer;
  }
  cout << "Total Items: " << TotalItemsInChain << endl;

}

/*
ItemID = 1 : Galaktokomika
ItemID = 2 : Zimarika
ItemID = 3 : Alantika
ItemID = 4 : Aporipantika
ItemID = 5 : Katharistika
*/

//Create the Items Array
void CreateItemsArray(ItemsToBuy ItemsArray[10]){
  ItemsArray[0].ItemID = 1;
  ItemsArray[0].ItemName = "Gala Evapore";
  ItemsArray[0].ItemPrice = 1.99;
  ItemsArray[1].ItemID = 1;
  ItemsArray[1].ItemName = "Gala olympos";
  ItemsArray[1].ItemPrice = 1.75;
  ItemsArray[2].ItemID = 2;
  ItemsArray[2].ItemName = "Makaronia Melissa";
  ItemsArray[2].ItemPrice = 2.20;
  ItemsArray[3].ItemID = 2;
  ItemsArray[3].ItemName = "Makaronia Misko";
  ItemsArray[3].ItemPrice = 2.30;
  ItemsArray[4].ItemID = 3;
  ItemsArray[4].ItemName = "Galopoula Ifantis";
  ItemsArray[4].ItemPrice = 2.10;
  ItemsArray[5].ItemID = 3;
  ItemsArray[5].ItemName = "Zabon Ifantis";
  ItemsArray[5].ItemPrice = 2.20;
  ItemsArray[6].ItemID = 4;
  ItemsArray[6].ItemName = "Katharistiko rouxwn Ariel";
  ItemsArray[6].ItemPrice = 2.50;
  ItemsArray[7].ItemID = 4;
  ItemsArray[7].ItemName = "Katharistiko rouxwn �urika";
  ItemsArray[7].ItemPrice = 2.40;
  ItemsArray[8].ItemID = 5;
  ItemsArray[8].ItemName = "Katharistiko Klinex";
  ItemsArray[8].ItemPrice = 2.50;
  ItemsArray[9].ItemID = 5;
  ItemsArray[9].ItemName = "Katharistiko Azax";
  ItemsArray[9].ItemPrice = 2.40;
}



void CheckNodeForExpress(CustomerChain Cashier[], int K, int L, int CashierPos){

  int NodeTotalItems = 0;
  CustomerNode *nodePointer = 0;

  Cashier[CashierPos].GetNodeInfo(1, NodeTotalItems, nodePointer);

  if(NodeTotalItems <= L){

    Cashier[K-1].AppendByForce(nodePointer);

    Cashier[CashierPos].DropNode(1);
  }

}

void MoveCustomerFromList(CustomerChain Cashier[], int K, int CashierPos){

    CustomerNode *nodePointer = 0;

    int NodeTotalItems = 0;

    Cashier[0].GetNodeInfo(0, NodeTotalItems, nodePointer);

    Cashier[CashierPos].AppendByForce(nodePointer);

    Cashier[0].DropNode(0);

}

int main(){

  //PreStart settings
   int SelectMode , Safety;
   srand(time(0));

   cout << "Select which mode will run (Must be between 1 and 3): ";
   cin >> Safety;
   while ( Safety != 1 && Safety != 2 && Safety != 3){
   	cout << "Wrong Input" << endl;
   	cout << "Give a value between 1 and 3: ";
   	cin >> Safety;
   }
   SelectMode=Safety;

   CreateItemsArray(ItemsArray);

   if(SelectMode == 1){

     //Case No1
     int K = 0; //Number of Cashiers
     cout << "Give the number of Cashiers: ";
     cin >> K;

     CustomerChain Cashier[K];

     bool ProgramEnds = false;
     while(!ProgramEnds){



       cout << "**********************************" << endl;
       cout << "Select an option from below: " << endl;
       cout << "1. Add new customer" << endl;
       cout << "2. Continue without adding a customer" << endl;
       cout << "3. Monitor Cashiers status" << endl;
       cout << "4. Monitor supermarket\'s data" << endl;
       cout << "5. Exit Program" << endl;
       cout << "Choose an option(1-5): ";

       int Select;
       cin >> Select;


       while(Select != 1 && Select != 2 && Select != 3 && Select != 4 && Select != 5){
         cout << "Choose an oprion(1-5): ";
         cin >> Select;
       }


       for(int i=0; i<K; i++){
         Cashier[i].CheckIfCustomerFinish();
       }

       if(Select == 1 || Select == 2){

        if(Select == 1){
          int Pos = 0;
          int Min = Cashier[0].GetTotalItems();

          for(int i=1; i<K; i++){
            if(Cashier[i].GetTotalItems() < Min){
              Min = Cashier[i].GetTotalItems();
              Pos = i;
            }
          }
          //Add user to Cashiers
          Cashier[Pos].Append();
        }

        RunTime += 10;

     }else if(Select == 3){



       cout << "**********************************" << endl;
       cout << "Current Time: " << RunTime << "sec" << endl;
       cout << "**********************************" << endl;
       for(int i=0; i<K; i++){
         cout << "Cashier " << i+1 << " Status: " << endl;
         cout << "Average waiting time: " << Cashier[i].GetLinesAvgTime() << " sec" << endl;
         Cashier[i].ShowChainsDetails();
       }
     }else if(Select == 4){

       Data.ShowSupermarketsData();

     }else{

       break;
       ProgramEnds = true;

     }

   }

   }else if(SelectMode == 2){
     //case No2
     int K = 0; //Number of Cashiers
     cout << "Give the number of Cashiers: ";
     cin >> K;

     K++;
     CustomerChain Cashier[K]; //Create K Cashiers and the waiting line

     bool ProgramEnds = false;

     while(!ProgramEnds){



       cout << "**********************************" << endl;
       cout << "Select an option from below: " << endl;
       cout << "1. Add new customer" << endl;
       cout << "2. Continue without adding a customer" << endl;
       cout << "3. Monitor Cashiers status" << endl;
       cout << "4. Monitor supermarket\'s data" << endl;
       cout << "5. Exit Program" << endl;
       cout << "Choose an option(1-5): ";

       int Select;
       cin >> Select;


       while(Select != 1 && Select != 2 && Select != 3 && Select != 4 && Select != 5){
         cout << "Choose an oprion(1-5): ";
         cin >> Select;
       }


       for(int i=1; i<K; i++){
         Cashier[i].CheckIfCustomerFinish();
       }

       if(Select == 1 || Select == 2){

        if(Select == 1){

            Cashier[0].Append(); //Add customer to waiting list
        }

        for(int i=1; i<K; i++){
            if(Cashier[i].Length() == 0 && Cashier[0].Length() != 0){
                MoveCustomerFromList(Cashier, K, i);
                break;
            }
        }

        RunTime += 10;

     }else if(Select == 3){


       cout << "**********************************" << endl;
       cout << "Current Time: " << RunTime << "sec" << endl;
       cout << "**********************************" << endl;

       cout << "Waiting Line Status: " << endl;
       cout << "Average waiting time: " << Cashier[0].GetLinesAvgTime() << " sec" << endl;
       Cashier[0].ShowChainsDetails();
       cout << "**********************************" << endl;

       for(int i=1; i<K; i++){
         cout << "Cashier " << i << " Status: " << endl;
         Cashier[i].ShowChainsDetails();
         cout << "**********************************" << endl;
       }


     }else if(Select == 4){

       Data.ShowSupermarketsData();

     }else{

       break;
       ProgramEnds = true;

     }

   }

   }else{
     //case No3
     int K = 0; //Number of Cashiers
     int L = 0; //Max items for express
     cout << "Give the number of Cashiers(Express cashier included): ";
     cin >> K;
     cout << "Give the max items for the express Cashier(The limit is 10): ";
     cin >> L;

     CustomerChain Cashier[K];

     bool ProgramEnds = false;
     while(!ProgramEnds){



       cout << "**********************************" << endl;
       cout << "Select an option from below: " << endl;
       cout << "1. Add new customer" << endl;
       cout << "2. Continue without adding a customer" << endl;
       cout << "3. Monitor Cashiers status" << endl;
       cout << "4. Monitor supermarket\'s data" << endl;
       cout << "5. Exit Program" << endl;
       cout << "Choose an option(1-5): ";

       int Select;
       cin >> Select;


       while(Select != 1 && Select != 2 && Select != 3 && Select != 4 && Select != 5){
         cout << "Choose an oprion(1-5): ";
         cin >> Select;
       }


       for(int i=0; i<K; i++){
         Cashier[i].CheckIfCustomerFinish();
       }

       if(Select == 1 || Select == 2){

        if(Select == 1){

          int Pos = 0;
          int Min = Cashier[0].GetTotalItems();

          for(int i=1; i<K-1; i++){
            if(Cashier[i].GetTotalItems() < Min){
              Min = Cashier[i].GetTotalItems();
              Pos = i;
            }
          }

          int ExpressItems = Cashier[K-1].GetTotalItems();

          if(Min < ExpressItems){

            //Add customer to Cashiers
            Cashier[Pos].Append();

          }else{

            //Add temp-node to Cashiers
            Cashier[Pos].Append();
            CheckNodeForExpress(Cashier, K, L, Pos);

          }


        }

        RunTime += 10;

     }else if(Select == 3){


       cout << "**********************************" << endl;
       cout << "Current Time: " << RunTime << "sec" << endl;
       cout << "**********************************" << endl;
       for(int i=0; i<K-1; i++){
         cout << "Cashier " << i+1 << " Status: " << endl;
         cout << "Average waiting time: " << Cashier[i].GetLinesAvgTime() << " sec" << endl;
         Cashier[i].ShowChainsDetails();
       }
       cout << "Express Cashier Status: " << endl;
       cout << "Average waiting time: " << Cashier[K-1].GetLinesAvgTime() << " sec" << endl;
       Cashier[K-1].ShowChainsDetails();


     }else if(Select == 4){

       Data.ShowSupermarketsData();

     }else{

       break;
       ProgramEnds = true;

     }

   }

   }



  return 0;
}
